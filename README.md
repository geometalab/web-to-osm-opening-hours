# Web to OSM Opening Hours

For evaluating opening hours (OH) in OpenStreetMap (OSM) according to the simplified specifiction see http://projets.pavie.info/yohours/ . 
There's a weblink to further information about this specification.

Demo websites: A' version https://codepen.io/AlejoFab/full/WLbBQr and  B's version https://codepen.io/BSee/full/WLbLGW 

For issues/bugs and feature requests see https://gitlab.com/geometalab/web-to-osm-opening-hours/issues

This is the current workflow behind this project: 

1. Open OpenStreetMap.org in browser and search the business there,
1. Check if opening hours are missing there, if yes edit OpenStreetMap.org.
1. Open new browser tab and search business website, where opening hours are indicated, 
1. Copy the opening hours free text from the business website,
1. Paste it in a website containing this code (e.g. https://codepen.io, tba.),
1. Website response shoul show opening hours in OSM format - copy this to paste buffer,
1. Head over to business on OpenStreetMap.org and paste buffer as value to opening_hours key.